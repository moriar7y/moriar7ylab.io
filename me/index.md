---
layout: about
---

Hello, my name is Matthew Moreno. I work in the security feild and am
currently working on developing a toolset for my very own distro, SlackHat
Linux. You can see my current progress by visiting the SlackHat group page
located [here](https://slackhat.gitlab.io/).

# How do I contact you?
You can reach me via email at matt@slackhat.org, or on 
[twitter](https://twitter.com/M0riar7y).

# What languages do you work in?
I primarily work with python, though I have been brushing up my skills with 
C. I really don't care for web development, though, so it's probably best 
if you don't ask.

# What's your favorite movie?
[Super Troopers](http://www.imdb.com/title/tt0247745/).

# That's all you have to say?
Yup.
