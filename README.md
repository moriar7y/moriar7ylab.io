[![build status](https://gitlab.com/moriar7y/moriar7y.gitlab.io/badges/master/build.svg)](https://gitlab.com/moriar7y/moriar7y.gitlab.io/commits/master)
![Jekyll Version](https://img.shields.io/gem/v/jekyll.svg)

---

Welcome to moriar7y's homepage!

There are still some things that need to be completed, but in general everything is going smoothly. 
I will be continuing to update the information on this page to ensure that it remains accurate.